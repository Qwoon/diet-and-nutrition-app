const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Create Schema 
const ProfileSchema = new Schema ({
	user: {
		type: Schema.Types.ObjectId,
		ref: 'users'
	},
	handle: {
		type: String,
		required: true,
		max: 40
	},
	age: {
		type: Number,
		required: true
	},
	sex: {
		type: String,
		required: true
	},
	weight: {
		type: Number,
		required: true
	},
	height: {
		type: Number,
		required: true
	},
	bmi: {
		type: Number,
		value: null
	},
	diet: {
		morning: [String],
		midday: [String],
		evening: [String]
	},
	dietPlanner: {
		morning: [String],
		midday: [String],
		evening: [String]
	},
	date: {
		type: Date,
		default: Date.now()
	}
});

module.exports = Profile = mongoose.model('profile', ProfileSchema);
