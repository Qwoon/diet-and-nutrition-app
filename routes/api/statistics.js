const express = require('express');
const router = express.Router();
const passport = require('passport');
const mongoose = require('mongoose');

const createStatisticsInfo = require('../../utils/statistics/statistics-info');

// Load Profile Model
const Profile = require('../../models/Profile');
// Load User Model
const User = require('../../models/User');


// @route   GET api/statistics
// @desc    Get current statistics
// @access  Private
router.get('/', passport.authenticate('jwt', {session: false}), (req, res) => {

	Profile.findOne({ user: req.user.id })
		.then(profile => {

			const {statistics_info, errors, isValid} = createStatisticsInfo(profile);

			res.status(200).json(statistics_info);
		})
		.catch(err => res.status(404).json(err));
	
});

module.exports = router;
