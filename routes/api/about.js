const express = require('express');
const router = express.Router();
const passport = require('passport');

router.get('/', passport.authenticate('jwt', { session: false}), (req, res) => {
    res.status(200).json({
        vk: 'https://vk.com/idskyple',
        twitter: 'https://twitter.com/Qwoontwit',
        instagram: 'https://www.instagram.com/artyom_gregory/?hl=bg'
    });
});

module.exports = router;