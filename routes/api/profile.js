const express = require('express');
const router = express.Router();
const passport = require('passport');

// Load Profile Validations
const validateProfileInput = require('../../Validation/profile');
// Load Diet-Planner Validation
const validateDietInput = require('../../Validation/diet-planner');

// Load Profile Model
const Profile = require('../../models/Profile');
// Load User Model
const User = require('../../models/User');

// @route   GET api/profile
// @desc    Get current users profile
// @access  Private
router.get('/', passport.authenticate('jwt', {session: false}), (req, res) => {
	const errors = {};

	Profile.findOne({ user: req.user.id })
		.populate('users', 'name')
		.then(profile => {
			if(!profile) {
				errors.noprofile = 'No profile found!';
				return res.status(404).json(errors);
			}
			res.json(profile);
		})
		.catch(err => res.status(404).json(err));

});

// @route   POST api/profile
// @desc    Create or edit user profile
// @access  Private
router.post('/', passport.authenticate('jwt', {session: false}), (req,res) => {

	const { errors, isValid } = validateProfileInput(req.body)

	// Check validation
	if(!isValid) {
		return res.status(404).json(errors);
	}

	// Get fields
		const profileFields = {};
		profileFields.user = req.user.id;
		if(req.body.handle) profileFields.handle = req.body.handle;
		if(req.body.age) profileFields.age = req.body.age;
		if(req.body.sex) profileFields.sex = req.body.sex;
		if(req.body.weight) profileFields.weight = req.body.weight;
		if(req.body.height) profileFields.height = req.body.height;
		profileFields.bmi = ((req.body.weight / req.body.height / req.body.height) * 10000).toFixed(2);


	Profile.findOne({ handle: profileFields.handle })
		.then(profile => {
				// Update
				if(profile) {
					Profile.findOneAndUpdate({ user: req.user.id }, {$set: profileFields}, {new: true})
						.then(profile => res.json(profile))
				} else {
					// Check if handle exists
					Profile.findOne({ handle: profileFields.handle})
						.then(profile => {
							if(profile) {
								errors.handle = 'This handle already exists';
								res.status(404).json(errors);
							}

							// Save new profile
							new Profile(profileFields).save().then(profile => res.json(profile));
						})
				}
			})


});

// @route   GET api/profile/diet
// @desc    Get Current Diet
// @access  Private
router.get('/diet', passport.authenticate('jwt', {session: false}), (req, res) => {
	const errors = {};

	Profile.findOne({ user: req.user.id })
		.then(profile => {

			const { diet } = profile;
			
			if(diet.morning.length === 0 && diet.midday.length === 0 && diet.evening.length ===0) {
				errors.noDiet = 'No diet found!';
				return res.status(404).json(errors);
			}
			res.json(diet);
		})
});


// @route   POST api/profile/diet
// @desc    Create or Edit Your Diet
// @access  Private
router.post('/diet', passport.authenticate('jwt', {session: false}), (req, res) => {

	const dietData = {};

	// Morning - Split into array
	if(typeof req.body.morning !== 'undefined') {
    	dietData.morning = req.body.morning.split(',');
	}

	// Midday - Split into array
	if(typeof req.body.midday !== 'undefined') {
		dietData.midday = req.body.midday.split(',');
	}

	// Evening - Split into array
	if(typeof req.body.evening !== 'undefined') {
    	dietData.evening = req.body.evening.split(',');
	}
	
	const { errors, isValid } = validateDietInput(dietData);

	// Check Validation
	if(!isValid) {
		res.status(404).json(errors);
	}

	Profile.findOne({ user: req.user.id })
		.then(profile => {
			if(profile) {
				Profile.findOneAndUpdate({ user: req.user.id }, { $set: { diet : dietData} }, {new: true})
					.then(profile => res.json(profile));
			} else {
				errors.noProfileFound = 'No profile found!';
				res.status(404).json(errors);
			}
		})

});

// @route   DELETE api/profile/diet
// @desc    Delete Diet
// @access  Private
router.delete('/diet', passport.authenticate('jwt', {session: false}), (req, res) =>{
	Profile.findOne({ user: req.user.id })
		.then(profile => {

			// Remove The Diet Planner
			profile.diet.morning.splice(0, profile.diet.morning.length);
			profile.diet.midday.splice(0, profile.diet.midday.length);
			profile.diet.evening.splice(0, profile.diet.evening.length);
			
			// Save
			profile.save().then(profile => res.json(profile));	
		})
		.catch(err => res.status(404).json(err));

});

// @route   GET api/profile/diet-planner
// @desc    Get Current Diet-Planner
// @access  Private
router.get('/diet-planner', passport.authenticate('jwt', {session: false}), (req, res) => {
	const errors = {};

	Profile.findOne({user: req.user.id})
		.then(profile => {

			const { dietPlanner } = profile;

			if(dietPlanner.morning.length === 0 && dietPlanner.midday.length === 0 && dietPlanner.evening.length === 0) {
				errors.noDietPlanner = 'No diet-planner found';
				return res.status(404).json(errors);
			}
			res.json(dietPlanner);
		})
		.catch(err => res.status(404).json(err));
});

// @route   POST api/profile/diet-planner
// @desc    Create or Edit Your Diet-Planner
// @access  Private
router.post('/diet-planner', passport.authenticate('jwt', {session: false}), (req, res) => {

	const dietData = {};

	 // Morning - Split into array
	 if(typeof req.body.morning !== 'undefined') {
        dietData.morning = req.body.morning.split(',');
	}

	 // Midday - Split into array
	 if(typeof req.body.midday !== 'undefined') {
        dietData.midday = req.body.midday.split(',');
	}

	 // Evening - Split into array
	 if(typeof req.body.evening !== 'undefined') {
        dietData.evening = req.body.evening.split(',');
	}

	const { errors, isValid } = validateDietInput(dietData);

	// Check Validation
	if(!isValid) {
		res.status(404).json(errors);
	}

	Profile.findOne({ user: req.user.id })
		.then(profile => {
			if(profile) {
				Profile.findOneAndUpdate({ user: req.user.id }, { $set: { dietPlanner : dietData} }, {new: true})
					.then(profile => res.json(profile));
			} else {
				errors.noProfileFound = 'No profile found!';
				res.status(404).json(errors);
			}
		})

});

// @route   DELETE api/profile/diet-planner
// @desc    Delete Diet-Planner
// @access  Private
router.delete('/diet-planner', passport.authenticate('jwt', {session: false}), (req, res) =>{
	Profile.findOne({ user: req.user.id })
		.then(profile => {

			// Remove The Diet Planner
			profile.dietPlanner.morning.splice(0, profile.dietPlanner.morning.length);
			profile.dietPlanner.midday.splice(0, profile.dietPlanner.midday.length);
			profile.dietPlanner.evening.splice(0, profile.dietPlanner.evening.length);
			
			// Save
			profile.save().then(profile => res.json(profile));	
		})
		.catch(err => res.status(404).json(err));
});

module.exports = router;