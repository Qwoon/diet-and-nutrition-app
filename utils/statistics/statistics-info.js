const isEmpty = require('../../validation/is-empty');


module.exports = function createStatisticsInfo(data) {
    
    let errors = {};
    let statistics_info = {};

    data.bmi = !isEmpty(data.bmi) ? data.bmi : null;
    
    if(isEmpty(data.bmi)) {
        errors.bmi = "BMI value is not declared!";
    }

    if(data.bmi < 18.5) {
        statistics_info.bmiClass = 'Underweight';
    } 

    if(data.bmi >= 18.5 && data.bmi <= 24.0) {
        statistics_info.bmiClass = 'Normal';
    }

    if(data.bmi >= 25 && data.bmi <= 29.9) {
        statistics_info.bmiClass = 'Overweight';
    }

    if(data.bmi >= 30) {
        statistics_info.bmiClass = 'Obese';
    }


    return {
        statistics_info,
        errors,
        isValid: isEmpty(errors)
    }
}

