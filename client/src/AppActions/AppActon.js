import * as d3 from 'd3';

import weightDataFile from '../data/weightData.csv';

import AppDispatcher from '../AppDispatchers/AppDispatcher';
import AppConstants from '../AppConstants/AppConstants';

export default { 
    handleSubmitData: (data) => {
        console.log(data);
        AppDispatcher.dispatch({
            actionType: AppConstants.RECIEVE_DATA,
            data: data,
            message: 'Got the data about the user'
        });
    },

    getWeightDataFile: () => {
        d3.queue()
            .defer(d3.tsv, weightDataFile)
            .await((err, result) => {
                AppDispatcher.dispatch({
                    actionType: AppConstants.SEND_WEIGHT_FILE_DATA,
                    data: result,
                    message: 'Sending the weight data file info'
                })
            })
    },

    addProductToStore: (product) => {
        AppDispatcher.dispatch({
            actionType: AppConstants.ADD_PRODUCT_TO_TABLE,
            data: product,
            message: 'Sending the added product to store'
        });
    },

    generateStatisticsText: (user) => {

    }
        

    

    
}