export default {
    generateStatisticsText: (userInfo) => {
        let text;
        if(userInfo.bmiClass == 'Underweight') {
            text = `With the weight of ${userInfo.weight} and the height of ${userInfo.height} you are below your recommended weight, with the BMI value of ${userInfo.bmiValue}. For more info, about how to fix the current issue, please read more....`;
        
        }else if(userInfo.bmiClass == 'Normal') {
            text = `With the weight of ${userInfo.weight} and the height of ${userInfo.height} you have a normal BMI value equal to ${userInfo.bmiValue}. For more info, about how to fix the current issue, please read more....`;
            
        }else if(userInfo.bmiClass == 'Overweight') {
            text = `With the weight of ${userInfo.weight} and the height of ${userInfo.height} you are above your recommended weight, with the BMI value of ${userInfo.bmiValue}. For more info, about how to fix the current issue, please read more....`;
            
        }else if(userInfo.bmiClass == 'Obese') {
            text = `The situation is pretty bad :(`;
            
        }
        return text;
    }
}