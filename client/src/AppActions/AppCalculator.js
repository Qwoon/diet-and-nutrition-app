import * as d3 from 'd3';

let bmiValue;

export default {
    calculateBMI(weight,height) {
        bmiValue = ((weight / height / height) * 10000).toFixed(2);
        return bmiValue;
    }
}