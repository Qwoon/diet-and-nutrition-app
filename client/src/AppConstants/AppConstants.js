import keyMirror from 'keymirror';

export default keyMirror({
    RECIEVE_DATA: null,
    RECIEVE_DATA_ERROR: null,
    SEND_DATA: null,
    SEND_DATA_ERROR: null,
    SEND_WEIGHT_FILE_DATA: null,
    SEND_WEIGHT_FILE_DATA_ERROR: null,
    ADD_PRODUCT_TO_TABLE: null,
    ADD_PRODUCT_TO_TABLE_ERROR: null,
    GENERATE_TEXT: null
})