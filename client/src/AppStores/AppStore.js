import { EventEmitter } from 'events';

import AppDispatcher from '../AppDispatchers/AppDispatcher';
import AppConstants from '../AppConstants/AppConstants';
import AppCalculator from '../AppActions/AppCalculator';
import AppTextGenerator from '../AppActions/AppTextGenerator';

const product = 'Product';

const plot = {
    CHANGE_EVENT: 'change',
    totalUserKcal: null,
    allProducts: [],
    selectedProducts: []
}

const user = {
    weight: null,
    height: null,
    bmiValue: null,
    bmiClass: 'Normal',
}



function calculateBMI(weight, height) {
    user.weight = weight;
    user.height = height;
    user.statisticsText;
    user.bmiValue = AppCalculator.calculateBMI(weight, height);
    console.log(user.bmiValue);
    if(user.bmiValue < 18.5 ){
        user.bmiClass = 'Underweight';
        console.log(user.bmiClass);
    }else if(user.bmiValue >= 18.5 && user.bmiValue <= 24.0) {
        user.bmiClass = 'Normal';
        console.log(user.bmiClass);
    }else if(user.bmiValue >= 25 && user.bmiValue <= 29.9) {
        user.bmiClass = 'Overweight';
        console.log(user.bmiClass);
    }else if(user.bmiValue >= 30) {
        user.bmiClass = 'Obese';
        console.log(user.bmiClass);
    }
    user.statisticsText = AppTextGenerator.generateStatisticsText(user);
}

function recieveWeightDataInfo(fileData) {
    fileData.forEach((item, i) => {
        plot.allProducts.push(fileData[i][product]);
    })
    //console.log(plot.allProducts);
}

function addProductToTable(product) {
    if(plot.allProducts.indexOf(product) > -1) {
        plot.selectedProducts.push(product);
    } else {
        alert('Sorry, invalid product name')
    }
    console.log(plot.selectedProducts);
}

class AppStoreClass extends EventEmitter {
    emitChange() {
        this.emit(plot.CHANGE_EVENT);
    }

    addChangeListener(callback) {
        this.on(plot.CHANGE_EVENT, callback);
    }

    removeChangeListener(callback) {
        this.on(plot.CHANGE_EVENT, callback);
    }

    getUserData() {
        return user; // Might delete the method in the future
    }

    getWeightData() {
        return plot.allProducts;
    }

    getUserStatistics() {
        return user.statisticsText;
    }

}

const AppStore = new AppStoreClass();

AppStore.dispatchToken = AppDispatcher.register(action => {
    switch (action.actionType) {
        case AppConstants.RECIEVE_DATA:
        calculateBMI(action.data.weight, action.data.height);
        AppStore.emitChange();
        break;

        case AppConstants.RECIEVE_DATA_ERROR:
        console.log('An error has occured');
        AppStore.emitChange();
        break;

        case AppConstants.SEND_WEIGHT_FILE_DATA:
        recieveWeightDataInfo(action.data);
        AppStore.emitChange();
        break;

        case AppConstants.ADD_PRODUCT_TO_TABLE:
        addProductToTable(action.data);
        AppStore.emitChange();
        break;

    }
});

export default AppStore;