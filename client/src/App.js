import React, { Component } from 'react';

import './App.css';

import logo from './FR.png'

import Theme from './Components/Theme';
import LandingPage from './Components/LandingPage';
import Footer from './Components/Footer';

import AppAction from './AppActions/AppActon';
import AppStore from './AppStores/AppStore';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '', 
      userWeight: undefined, 
      userHeight: undefined,
      submitted: false
     };
     this.changeSubmitState = this.changeSubmitState.bind(this);
  }

  changeSubmitState(value) {
    this.setState({submitted: value});
  }

  componentWillMount() {
    AppAction.getWeightDataFile();
  }


  render() {
    let landing, statistics;
      if(this.state.submitted === false){
        landing = <LandingPage changeSubmitState={this.changeSubmitState}/>;

        statistics = "";

      }else if(this.state.submitted === true) {
        landing = "";

        statistics = <Theme />;
      }
      
    return (
      <div className="App">
        <header className="App-header">
          <img className="main-logo" src={logo} alt="logo" />
        </header>
          {landing}
          {statistics}
        <Footer />
      </div>
    );
  }
}

export default App;
