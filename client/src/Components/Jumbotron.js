import React, { Component } from 'react';

import AppStore from '../AppStores/AppStore';


class Jumbotron extends Component {
    constructor(props) {
      super(props);
      this.state = {
        products: props.products
      };
      
    }


    render() {
      let products;
      if(this.state.products) {
        products = this.state.products.map(product => {
          return <div className="well event-product-name" key={this.state.products.indexOf(product)}>{product}</div>
        });
      }
        return(
          <div className="jumbotron">
            <div className="row">
              <div className="col-md-6">Chart</div>
              <div className="col-md-6">{products}</div>
            </div>
          </div>  
        );
    }
}

export default Jumbotron;