import React, { Component } from 'react';

import AppAction from '../AppActions/AppActon';

class LandingPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            weight: undefined,
            height: undefined
        }
        
    }

    handleSubmit(e) {
        if(this.refs.userName.value && this.refs.userWeight.value && this.refs.userHeight.value){
            this.setState({
                name: this.refs.userName.value,
                weight: this.refs.userWeight.value,
                height: this.refs.userHeight.value
            }, () => {
                AppAction.handleSubmitData(this.state);
                this.props.changeSubmitState(true);
            });
            e.preventDefault();
        }else {
            alert('Enter all the input fields below!');
            this.props.changeSubmitState(false);
        }
        
    }


    render() {
        return(
            <div className="landing-page">
                <div className="landing-text-area">
                    <h1 className="welcome-text">Welcome to my little off-side project called "FoodRgin"</h1>
                    <h2 className="reg-text">Follow the registration form below</h2>
                </div>
                <div className="row">
                <form onSubmit={this.handleSubmit.bind(this)} className="intro-form">
                    <div className="form-group col-xs-7 col-md-offset-5">
                        <input type="text" placeholder="Enter your name..." ref="userName" />
                    </div>

                    <div className="form-group col-xs-7 col-md-offset-5">
                        <input type="number" placeholder="Enter your weight..." ref="userWeight" />
                    </div>

                    <div className="form-group col-xs-7 col-md-offset-5">
                        <input type="number" placeholder="Enter your height..." ref="userHeight" />
                    </div>

                    <br />
                    <div className="form-group col-xs-7 col-md-offset-5">
                        <input className="btn btn-success" type="submit" value="Submit" />  
                    </div>       
                </form>
                </div>
            </div>
        );
    }

}

export default LandingPage;