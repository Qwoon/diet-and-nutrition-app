import React, { Component } from 'react';

class Footer extends Component { 
    render() {
        return(
            <div className="footer">
                <div className="row">
                    <div className="col-sm-6">
                        <h3><span>Contact Us:</span></h3>
                        <ul>
                            <a href="https://vk.com/idskyple"><i className="fab fa-vk"> Vkontakte  </i></a>
                            <a href="https://web.telegram.org/#/im?p=@Artyom_Gregory"><i className="fab fa-telegram"> Telegram </i></a>
                            <a href="https://www.facebook.com/profile.php?id=100006914898881"><i className="fab fa-facebook-f"> Facebook </i></a>
                        </ul>
                    </div>
                    <div className="col-sm-6">
                        <h3><span>About the project:</span></h3>
                        <p>Everything was created by Artyom "Qwoon" Gregory. The project was made for non-commercials purposes. No copyrights 2018.</p>

                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;