import React, { Component } from 'react';

class Navbar extends Component {
    constructor(props){
        super(props);
        this.brand = this.props.brand;

    }

    change(page) {
        this.props.change(page);
    }

    render(){
        return(
            <nav className="navbar navbar-default">
                <div className="container">
                    <div className="navbar-header">
                        <a className="navbar-brand" href="#">Foodrgin {this.brand}</a>
                    </div>
                    <div id="navbar">
                        <ul className="nav navbar-nav">
                            <li className={(this.props.currentPage === 'statistics') ? 'active' : '' }><a onClick={this.change.bind(this, 'statistics')} href="#">Statistics</a></li>
                            <li className={(this.props.currentPage === 'recommendations') ? 'active' : '' }><a onClick={this.change.bind(this, 'recommendations')} href="#">Recommendations</a></li>
                            <li className={(this.props.currentPage === 'about') ? 'active' : '' }><a onClick={this.change.bind(this, 'about')} href="#">About Foodrgin</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Navbar;