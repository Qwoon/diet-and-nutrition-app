import React, { Component } from 'react';
import {Well} from 'react-bootstrap';

import AppStore from '../AppStores/AppStore';
import AppActon from '../AppActions/AppActon';


class Pagebody extends Component {
    constructor(props) {
        super(props);
        this.state = {
            foodInput: 'Salami',
            newProduct: ''

        }
    }

        

    getWeightFileData() {
        this.setState({foodOutput : AppStore.getWeightData()}, ()=> {
            console.log(this.state.foodOutput);
        });
    }

    
    handleChange(e) {
        this.setState({foodInput: e.target.value});
        console.log(this.state.foodInput);
    }
    

    handleSubmit(e) {
        if(this.refs.food.value === "") {
            alert('You need to enter a product!');
        }else {
            this.setState({newProduct: this.refs.food.value}, () => {
                this.props.addProduct(this.state.newProduct);
                AppActon.addProductToStore(this.refs.food.value)
            })
        }
        e.target.reset();
        e.preventDefault();
    }

    render() {
        let firstPageContent;
        let secondPageContent;
        let searchInput;
        if(this.props.currentPage === 'statistics') {
            firstPageContent = 'Tell us what are you usually eating in a day? (All products are calculated on 100g mass)';
            secondPageContent = this.props.userStatistics;
            searchInput = <form onSubmit={this.handleSubmit.bind(this)} onChange={this.handleChange.bind(this)}><Well><input type="text" placeholder="Enter the food you are eating" ref="food"></input></Well></form>
        } else if(this.props.currentPage === 'recommendations') {
            firstPageContent = 'This is the RECOMMENDATIONS lorem ipsum dilor sit amet consectetur adipicicing elit';
        } else if(this.props.currentPage === 'about') {
            firstPageContent = 'This is the ABOUT US PAGE lorem ipsum dilor sit amet consectetur adipisicing elit';
        } else {
            firstPageContent = 'Page not found :('
        }

        return(
            <div className="pagebody">
            <h1> This is the {this.props.currentPage} page body</h1>
            <p>{firstPageContent}</p>
                <div className="row">
                    <div className="col-md-6">
                        {searchInput}
                    </div>
                    <div className="col-md-6">
                        {secondPageContent}
                    </div>
                </div>
            </div>
        );
    }
    
}

export default Pagebody;