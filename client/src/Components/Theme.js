import React, { Component } from 'react';
 
import Navbar from './Navbar';
import Jumbotron from './Jumbotron';
import Pagebody from './Pagebody';

import AppAction from '../AppActions/AppActon';
import AppStore from '../AppStores/AppStore';

function checkItemInArray() {
    
}


class Theme extends Component {
    constructor(props) {
        super(props);
        this.brand = 'by Qwoon'; 
        this.state = {
            currentPage: 'statistics',
            foodInput: '',
            products: [],
            userStatistics: AppStore.getUserStatistics()
        };

        this.handleChange = this.handleChange.bind(this);

    }

    handleChange(page) {
        this.setState({currentPage: page});
    }

    handleAddProduct(product) {
        let products = this.state.products;
        products.push(product);
        this.setState({products: products});
        console.log(this.state);
    }

    render() {
        let jumbotron;
        if(this.state.currentPage == 'statistics'){
            jumbotron = <Jumbotron products={this.state.products} /> ;
        } else {
            jumbotron = '';
        }

        return(
            <div className='container'>
                <Navbar brand={this.brand} currentPage={this.state.currentPage} change={this.handleChange}/>
                {jumbotron}
                <Pagebody currentPage={this.state.currentPage} addProduct={this.handleAddProduct.bind(this)} userStatistics={this.state.userStatistics} />
            </div>
        );
    }
}

export default Theme;