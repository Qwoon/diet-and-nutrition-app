const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const passport = require('passport');

const users = require('./routes/api/users');
const profile = require('./routes/api/profile');
const statistics = require('./routes/api/statistics');
const about = require('./routes/api/about');

const app = express();

// Body Parser Middleware 
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
	.connect(db, {useNewUrlParser: true})
	.then(() => console.log('MongoDB Started'))
	.catch(err => console.log(err));

// Passport Middleware
app.use(passport.initialize());

// Passport Config
require('./config/passport')(passport);

//Use API Routes
app.use('/api/users', users);
app.use('/api/profile', profile);
app.use('/api/statistics', statistics);
//app.use('/api/about', about);

const port = process.env.PORT || 5000;

// Listening to port
app.listen(port, () => console.log(`Running on port ${port}`));
