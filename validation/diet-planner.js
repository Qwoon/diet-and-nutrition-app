const isEmpty = require('./is-empty');


module.exports = function validateDietInput (data) {
	let errors = {};

	data.morning = !isEmpty(data.morning) ? data.morning : '';
	data.midday = !isEmpty(data.midday) ? data.midday : '';
	data.evening = !isEmpty(data.evening) ? data.evening : '';

	if(isEmpty(data.morning)) {
		errors.morning = 'You must select morning food!';
	}

	if(data.morning.length < 3) {
		errors.morning = 'You need to select at least 3 products for morning!';
	}

	if(isEmpty(data.midday)) {
		errors.midday = 'You must select midday food!';
	}

	if(data.midday.length < 3) {
		errors.midday = 'You need to select at least 3 products for midday!';
	}

	if(isEmpty(data.evening)) {
		errors.evening = 'You must select evening food!';
	}

	if(data.evening.length < 3) {
		errors.evening = 'You need to select at least 3 products for evening!';
	}

	return {
		errors, 
		isValid: isEmpty(errors)
	};
} 



