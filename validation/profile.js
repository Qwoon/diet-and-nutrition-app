const isEmpty = require('./is-empty');
const Validator = require('validator');

module.exports = function validateProfileInput(data) {
	let errors = {};

	data.handle = !isEmpty(data.handle) ? data.handle : '';
	data.age = !isEmpty(data.age) ? data.age : '';
	data.sex = !isEmpty(data.sex) ? data.sex : '';
	data.weight = !isEmpty(data.weight) ? data.weight : '';
	data.height = !isEmpty(data.height) ? data.height : '';


	if(!Validator.isLength(data.handle, {min: 2, max: 30} )) {
		errors.handle = 'Handle needs to be between 2 and 30 characters';
	}

	if(Validator.isEmpty(data.handle)) {
		errors.handle = 'Profile handle is required';
	}

	if(!Validator.isInt(data.age, { min: 14, max: 90 })) {
		errors.age = 'Your age must be between 14 and 90 years!';
	}

	if(Validator.isEmpty(data.age)) {
		errors.age = 'Age field must be specified!';
	}

	if(Validator.isEmpty(data.sex)) {
		errors.sex = 'Sex must be specified!';
	}

	if(!Validator.isInt(data.weight, {min: 20, max: 200})) {
		errors.weight = 'Weight must be between 20 and 200 kg!';
	}

	if(Validator.isEmpty(data.weight)) {
		errors.weight = 'Weight field is required';
	}

	if(!Validator.isInt(data.height, {min: 20, max: 200})) {
		errors.height = 'Height must be between 20 and 200 m!';
	}

	if(Validator.isEmpty(data.height)) {
		errors.height = 'Height field is required';
	}

	return {
		errors,
		isValid: isEmpty(errors)
	}
}